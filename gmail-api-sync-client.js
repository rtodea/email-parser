const gmailApiSync = require('gmail-api-sync');
const token = require('./token.json');

gmailApiSync.setClientSecretsFile('./client_secret.json');
gmailApiSync.authorizeWithToken(token.access_token, (err, oauth) => {
    console.log(oauth);
    const options = {
        query: 'from: laravel@indiamike.com',
    };
    gmailApiSync.queryMessages(oauth, options, (err, response) => {
        console.log(response);
    });
});
