const token = require('./token.json');
const Gmail = require('node-gmail-api');
const HtmlParser = require('node-html-parser');
const ramda = require('ramda');


const gmail = new Gmail(token.access_token);
const stream = gmail.messages('from: laravel@indiamike.com', {max: 400});
const registrations = [];
stream.on('data', (d) => {
    const subject = d.payload.headers.find(({name}) => ('Subject' === name)).value;
    const date = d.payload.headers.find(({name}) => ('Date' === name)).value;
    const actualMessage = new Buffer(d.payload.body.data, 'base64').toString('utf8');
    const dom = HtmlParser.parse(actualMessage);
    const registration = JSON.parse(dom.querySelector('h4').text);
    registrations.push(Object.assign({subject, date}, registration));
});
stream.on('end', () => {
    console.log(ramda);
    console.log(registrations.length);
});
